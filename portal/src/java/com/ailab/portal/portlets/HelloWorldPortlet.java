package com.ailab.portal.portlets;
    
//jetspeed
import org.apache.jetspeed.portal.*;
import org.apache.jetspeed.portal.portlets.*;
import org.apache.jetspeed.portal.expire.*;
import org.apache.jetspeed.capability.*;
import org.apache.jetspeed.util.*;
import org.apache.jetspeed.services.portletcache.Cacheable;
import org.apache.jetspeed.portal.service.PersistenceService;
import org.apache.jetspeed.portal.service.ServiceFactory;
import org.apache.jetspeed.portal.service.ServiceException;
import org.apache.jetspeed.services.Registry;
import org.apache.jetspeed.om.newregistry.PortletEntry;
import org.apache.jetspeed.om.newregistry.MediaTypeEntry;

//ecs
import org.apache.ecs.*;

//turbine stuff
import org.apache.turbine.util.*;

//java stuff
import java.util.*;
import java.io.*;

/**
<p>
Test portlet
</p>

@author <A HREF="mailto:hyoon@ailab.com">Hyoungsoo Yoon</A>
@version $Id: HelloWorldPortlet.java,v 1.1.1.1 2001/06/26 05:47:41 cvs Exp $
*/
public class HelloWorldPortlet extends AbstractPortlet
{
    
    /**
    */
    public ConcreteElement getContent( RunData rundata ) {

        return (new StringElement ("Hello World"));
    }
    
}
