@echo off

REM ----------------------------------------------------------
REM AILab Portal Build Script
REM This script is tested under Windows NT & Win98
REM ----------------------------------------------------------

setlocal

REM -- root directory for the project
set _PROJECTDIR=%PROJECTDIR%
set PROJECTDIR=F:\ailab\portal

REM -- Directory containing jars required for runtime
set _LIBDIR=%LIBDIR%
set LIBDIR=%PROJECTDIR%\lib

REM -- Build directory
set _BUILDDIR=%BUILDDIR%
set BUILDDIR=%PROJECTDIR%\bin

REM -- Directory containing jars required for compilation
set _BUILDLIBDIR=%BUILDLIBDIR%
set BUILDLIBDIR=%BUILDDIR%\lib

REM -- Name of the build file to use
set _BUILDFILE=%BUILDFILE%
set BUILDFILE=%BUILDDIR%\build.xml

REM --------------------------------------------
REM No need to edit anything past here
REM --------------------------------------------

:init
set _CLASSPATH=%CLASSPATH%
set LOCALPATH=.

:buildpath
for %%l IN (%LIBDIR%\*.jar) DO call %BUILDDIR%\setlocalpath %%l
for %%l IN (%BUILDLIBDIR%\*.jar) DO call %BUILDDIR%\setlocalpath %%l
for %%l IN (%JAR_DEPENDENCIES%) DO call %BUILDDIR%\setlocalpath %%l

:testjavahome
if "%JAVA_HOME%" == "" goto setjavahome
goto build

:setjavahome
if not "%OS%" == "Windows_NT" goto javahomeerror

:setjavahoment
for %%j IN (java.exe) DO set JAVABIN=%%~dp$PATH:j
if "%JAVABIN%" == "" goto javahomeerror
for %%j IN (%JAVABIN%..\) DO set JAVA_HOME=%%~dpj

:build
if exist %JAVA_HOME%\lib\tools.jar set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar

%JAVA_HOME%\bin\java.exe -classpath "%LOCALPATH%;%CLASSPATH%" org.apache.tools.ant.Main -buildfile %BUILDFILE% %1

goto end

:javahomeerror
echo ERROR: JAVA_HOME not found in your environment.
echo Please, set the JAVA_HOME variable in your environment to match the
echo location of the Java Virtual Machine you want to use.

:end

set CLASSPATH=%_CLASSPATH%
set PROJECTDIR=%_PROJECTDIR%
set LIBDIR=%_LIBDIR%
set BUILDDIR=%_BUILDDIR%
set BUILDLIBDIR=%_BUILDLIBDIR%
set BUILDFILE=%_BUILDFILE%
set JAR_DEPENDENCIES=%_JAR_DEPENDENCIES%

endlocal
