<%@ page import="org.apache.jetspeed.util.*" %>
<jsp:useBean id="databean" class="org.apache.jetspeed.portal.portlets.customize.CustomizeBean" scope="request"/>


<br>
<table  width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
      <h2>Customize Home Page</h2>

      Please check which portlets you want to see on which device.
      
      <!-- The input form -->		  
      <form method="post" action="<%=databean.getPage2URL()%>">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr valign="top"> 
            <td width="150"></td>
            <td align="center" width="70"><b>Web</b></td>
            <td align="center" width="70"><b>WAP Phone</b></td>
            <td> &nbsp;</td>
          </tr>
          <% for( int i=0; i<databean.size(); i++ ) { 
             org.apache.jetspeed.portal.portlets.customize.PortletDataBean portletbean = databean.getDataBeanAt( i );
          %>

          <tr valign="middle"> 
            <td  width="150"><%=portletbean.getTitle()%></td>
            <% 
            if( portletbean.getHTMLCapable() )
            { %>
            <td  align="center" width="70"><input type="checkbox"    name="<%="checkHTML" +portletbean.getName()%>" value="<%=portletbean.getName()%>" <%=portletbean.getHTMLChecked()%>></td>
            <% } else { %>
            <td  align="center" width="70">  </td>
            <% }
            if( portletbean.getWMLCapable() )
            { %>
            <td  align="center" width="70"><input type="checkbox"    name="<%="checkWML" +portletbean.getName()%>" value="<%=portletbean.getName()%>" <%=portletbean.getWMLChecked()%>></td>
            <% } else { %>
            <td  align="center" width="70">  </td>
            <% } %>
            <td>&nbsp;</td>
          </tr>
          <% } %>  
 
        </table>
        <p align="center">
          <input type="submit" name="Submit" value="Finish">
          <input type="button" name="Submit2" value="Cancel" onClick="document.location.href='<%=databean.getHomeURL()%>'">
        </p>
      </form>
    </td>
  </tr>
</table>
