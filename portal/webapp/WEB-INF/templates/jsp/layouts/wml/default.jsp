<?xml version="1.0"?><!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.1//EN" "http://www.wapforum.org/DTD/wml_1.1.xml">
<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<% String screenJsp = (String)request.getAttribute("screenJsp"); %>

<wml>
  <jetspeed:navigation  defaultTemplate="wml/top.jsp" />
  <jsp:include page="<%= screenJsp %>" flush="true" /> 
  <jetspeed:navigation  defaultTemplate="wml/bottom.jsp" />
</wml>
