<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<% String screenJsp = (String)request.getAttribute("screenJsp"); %>

<html>

<head>
 <base href="<jetspeed:uriLookup type="BaseURL" />">
 <link href="css/default.css" type="text/css" rel="stylesheet"> 
</head>


<body bgcolor="#ffffff">

<table cellspacing="0" width="100%" border="0" cellpadding="0">
  <tr>
    <td>
      <img src="<jetspeed:contentUri href="images/jetspeed-logo.gif" />">
    </td>
    <td>
      <jetspeed:navigation  defaultTemplate="html/top_default.jsp"  loggedInTemplate="html/top_loggedIn.jsp" />
    </td>
  </tr>
</table>

<table cellspacing="0" width="100%" cellpadding="0" border="0">
  <tr>
    <td valign="top" bgcolor="#ffffff">
   
      <jetspeed:navigation defaultTemplate="html/left.jsp" />

<%-- Enable for Debugging ("Included servlet Error: 500")
Including:<%= screenJsp %> <p>
--%>
      <jsp:include page="<%= screenJsp %>" flush="true" /> 
 
    </td>
  </tr>
</table>

<table cellspacing="0" width="100%" cellpadding="0" border="0">
  <tr>
    <td valign="bottom" bgcolor="#ffffff">
      <jetspeed:navigation  defaultTemplate="html/bottom.jsp" />
    </td>
  </tr>
</table>

</body>

</html>
