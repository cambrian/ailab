<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%@ page import = "org.apache.turbine.util.*" %> 

<% RunData rundata = (RunData)request.getAttribute("rundata"); %>


<div align="left">

<table cellpadding="4" cellspacing="0" border="0">

<td>
  <h2>Neuen Zugang einrichten</h2>

  F�llen Sie bitte folgende Felder aus, um einen neuen Zugang einzurichten. <br />
  Sobald der Zugang eingerichtet ist, werden Sie einen Best�tigungsmail mit einem Schl�ssel erhalten. <br />
  Diesen Schl�ssel m�ssen Sie dann auf der folgenden Seite eintragen, um Ihren neuen Zugang endg�ltig einzurichten. <br />
  <b>Eine g�ltige Email-Adresse ist daher zwingend n�tig!</b><br/>

  <form accept-charset="UTF-8, ISO-8859-1" 
        method="POST" 
        action="<jetspeed:link template="ConfirmRegistration" />" 
        enctype="application/x-www-form-urlencoded">
  <input name="action" type="hidden" value="CreateNewUserAndConfirm">

<%
  String message = rundata.getMessage();
  if (message != null) {
%>
    <b> <%= message %> </b> <p />
<%
  }  
%>

  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td> Benutzername: </td>
      <td> <input name="username" type="TEXT" value="<%= rundata.getParameters().getString("username", "") %>"> </td>
    </tr>
    <tr>
      <td> Passwort: </td>
      <td> <input name="password" type="PASSWORD" value=""> </td>
    </tr>
    <tr>
      <td> Passwort (Best�tigung): </td>
      <td> <input name="password_confirm" type="PASSWORD" value=""> </td>
    </tr>
    <tr>
      <td colspan="2"> <hr size="1" noshade> </td>
    </tr>
    <tr>
      <td> Vorname: </td>
      <td> <input name="firstname" type="TEXT" value="<%= rundata.getParameters().getString("firstname", "") %>"> </td> 
    </tr>
    <tr>
      <td> Nachname: </td>
      <td> <input name="lastname" type="TEXT" value="<%= rundata.getParameters().getString("lastname", "") %>"> </td>
    </tr>
    <tr>
      <td> Email: </td>
      <td> <input name="email" type="TEXT" value="<%= rundata.getParameters().getString("email", "") %>"> </td>
    </tr> 
    <tr>
      <td colspan="2" align="right">
        <br />
        <input name="submit1" type="reset"  value="Felder leeren">
        &nbsp;
        <input name="submit2" type="submit" value="Neuen Zugang einrichten">
      </td>
    </tr>
  </table>

</td>
</table>

</form>

</div>
