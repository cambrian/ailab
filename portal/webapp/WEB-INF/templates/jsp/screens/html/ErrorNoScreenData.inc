<%-- This screen be shown (using "include file") by other screens if they can't find the ScreenData hashtable  --%>

<table border=1 cellpadding="5"> 

  <tr> 
    <td>
      <br>
      <h2>The screen tmplate couldn't find its ScreenData in the context!</h2>
      It seems as if the PrepareScreen action hasn't been executed. Please check if the requested URL is correct.
    </td>
  </tr> 

</table>
