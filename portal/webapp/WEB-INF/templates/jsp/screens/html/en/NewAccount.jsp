<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%@ page import = "org.apache.turbine.util.*" %> 

<% RunData rundata = (RunData)request.getAttribute("rundata"); %>


<div align="left">

<table cellpadding="4" cellspacing="0" border="0">

<td>
  <h2>Create a new Jetspeed account</h2>

  To create a new account, please fill in all of the fields below. <br />
  After your account has been created, you will receive an email with a confirmation key. <br />
  Enter the confirmation key that you received in the email on the next screen in order to confirm your registration with the system. <br />
  <b>A valid email address is required!</b><br/>

  <form accept-charset="UTF-8, ISO-8859-1" 
        method="POST" 
        action="<jetspeed:link template="ConfirmRegistration" />" 
        enctype="application/x-www-form-urlencoded">
  <input name="action" type="hidden" value="CreateNewUserAndConfirm">

<%
  String message = rundata.getMessage();
  if (message != null) {
%>
    <b> <%= message %> </b> <p />
<%
  }  
%>

  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td> Username: </td>
      <td> <input name="username" type="TEXT" value="<%= rundata.getParameters().getString("username", "") %>"> </td>
    </tr>
    <tr>
      <td> Password: </td>
      <td> <input name="password" type="PASSWORD" value=""> </td>
    </tr>
    <tr>
      <td> Password (confirm): </td>
      <td> <input name="password_confirm" type="PASSWORD" value=""> </td>
    </tr>
    <tr>
      <td colspan="2"> <hr size="1" noshade> </td>
    </tr>
    <tr>
      <td> First Name: </td>
      <td> <input name="firstname" type="TEXT" value="<%= rundata.getParameters().getString("firstname", "") %>"> </td> 
    </tr>
    <tr>
      <td> Last Name: </td>
      <td> <input name="lastname" type="TEXT" value="<%= rundata.getParameters().getString("lastname", "") %>"> </td>
    </tr>
    <tr>
      <td> Email: </td>
      <td> <input name="email" type="TEXT" value="<%= rundata.getParameters().getString("email", "") %>"> </td>
    </tr> 
    <tr>
      <td colspan="2" align="right">
        <br />
        <input name="submit1" type="reset"  value="Reset">
        &nbsp;
        <input name="submit2" type="submit" value="Create New Account">
      </td>
    </tr>
  </table>

</td>
</table>

</form>

</div>
