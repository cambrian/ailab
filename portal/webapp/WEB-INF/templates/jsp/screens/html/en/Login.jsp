<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%@ page import = "org.apache.turbine.util.*" %> 
<%@ page import = "org.apache.turbine.services.resources.TurbineResources" %> 

<% RunData rundata = (RunData)request.getAttribute("rundata"); %>

<div align="left">
<form method="POST" 
      action="<jetspeed:dynamicUri/>" 
      enctype="application/x-www-form-urlencoded">
<input name="action" type="hidden" value="<%= TurbineResources.getString( "action.login" ) %>">

<br>
<table border="0" cellspacing="2" cellpadding="1">
<tr>
  <td>Username:</td>
  <td><input size="12" value="<%= rundata.getParameters().getString("username", "") %>" name="username" maxlength="25" type="text"></td>
</tr>
<tr>
  <td>Password:</td>
  <td><input size="12" value="" name="password" maxlength="25" type="password"></td>
</tr>
<tr>
  <td colspan="2" align="center">
    <input name="submit" type="submit" value="Login">
  </td>
</tr>
<tr>
  <td colspan="2" align="center">
    <small><a href="<jetspeed:uriLookup type="Enrollment" />">Create a new account</a></small>
  </td>
</tr>
</table>
<br>

</form>
</div>
