<%-- This screen will be shown if the user tries to access a screen that only logged in users may see --%>

<table border=1 cellpadding="5"> 

  <tr> 
    <td>
      <br>
      <h2>Please login first!</h2>
       Sorry, you must be logged in in order to access this screen.
    </td>
  </tr> 

</table>
