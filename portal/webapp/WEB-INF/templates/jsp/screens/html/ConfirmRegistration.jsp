<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%@ page import = "org.apache.turbine.util.*" %> 
<%@ page import = "org.apache.turbine.services.resources.TurbineResources" %> 

<% RunData rundata = (RunData)request.getAttribute("rundata"); %>

<div align="left">

  <form accept-charset="UTF-8, ISO-8859-1" 
      method="POST" 
      action="<jetspeed:dynamicUri/>" 
      enctype="application/x-www-form-urlencoded">
    <input name="username" type="hidden" value="<%= rundata.getParameters().getString("username", "") %>">
    <input name="action" type="hidden" value="<%= TurbineResources.getString( "action.login" ) %>">

Please check your email and enter your secret key to confirm your registration: <p />
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td> Secret Key: </td>
    <td align="right">
      <input name="secretkey" type="TEXT" value="">
    </td>
  </tr>
  <tr>
    <td colspan="2"> &nbsp; </td>
  </tr>
  <tr>
    <td colspan="2" align="right">
      <input name="reset"   type="reset"  value="Reset">
      <input name="submit2" type="submit" value="Confirm Registration">
    </td>
  </tr>
</table>

</form>

</div>
