<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%@ page import = "org.apache.turbine.util.RunData" %> 
<%@ page import = "java.util.Hashtable" %> 

<% 
    String username  = "";
    String firstname = "";
    String lastname  = "";
    String email     = "";

    try {
        Hashtable screenData = (Hashtable)request.getAttribute( "ScreenDataEditAccount" );

        username  = (String)screenData.get( "username" );
        firstname = (String)screenData.get( "firstname" );
        lastname  = (String)screenData.get( "lastname" );
        email     = (String)screenData.get( "email" );
    } catch (Exception e) {
%>
        <%@ include file="ErrorNoScreenData.inc" %>
<%
        return;
    }

%>

<div align="left">

<table cellpadding="4" cellspacing="0" border="0">

<td>
  <h2><u>Edit your account details</u></h2>

  <form accept-charset="UTF-8, ISO-8859-1" 
        method="POST" 
        action="<jetspeed:dynamicUri/>" 
        enctype="application/x-www-form-urlencoded">
  <input name="action" type="hidden" value="UpdateAccount">
  <input name="username" type="hidden" value="<%= username %>">

<%
  String message = ((RunData)request.getAttribute("rundata")).getMessage();
  if (message != null) {
%>
    <p><b> <%= message %> </b></p>
<%
  }  
%>

  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td> Username: </td>
      <td> <%= username %> </td>
    </tr>
    <tr>
      <td> Password: </td>
      <td> <input name="password" type="PASSWORD" value=""> </td>
    </tr>
    <tr>
      <td> Password (confirm): </td>
      <td> <input name="password_confirm" type="PASSWORD" value=""> </td>
    </tr>
    <tr>
      <td colspan="2"> <hr size="1" noshade> </td>
    </tr>
    <tr>
      <td> First Name: </td>
      <td> <input name="firstname" type="TEXT" value="<%= firstname %>"> </td> 
    </tr>
    <tr>
      <td> Last Name: </td>
      <td> <input name="lastname" type="TEXT" value="<%= lastname %>"> </td>
    </tr>
    <tr>
      <td> Email: </td>
      <td> <input name="email" type="TEXT" value="<%= email %>"> </td>
    </tr> 
    <tr>
      <td colspan="2" align="right">
        <br />
        <input name="cancelBtn" type="submit" value="Cancel">
        &nbsp;
        <input name="submit2"   type="submit" value="Update Account">
      </td>
    </tr>
  </table>

</td>
</table>

</form>

</div>
