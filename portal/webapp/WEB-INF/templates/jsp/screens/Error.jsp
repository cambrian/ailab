<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%-- The default error screen that will be displayed if no specific screen is set --%>

There has been an application error!
An appropriate error screen template is not defined or couldn't be found!
