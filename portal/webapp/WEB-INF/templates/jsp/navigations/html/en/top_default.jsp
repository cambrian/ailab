<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<div>
  Welcome to Jetspeed<br />
  <a href="<jetspeed:uriLookup type="Login" />">Please login to Jetspeed</a>
  <br />
  <b>Server date:</b> <jetspeed:info requestedInfo="ServerDate" /><br />
</div>
