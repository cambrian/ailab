<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<div>
  Welcome to Jetspeed -> 
  <a href="<jetspeed:uriLookup type="Home" />">Home</a>
  <br>
  <a href="<jetspeed:uriLookup type="Customize" />">Customize</a>
  &nbsp;&nbsp;&nbsp;
<!--
  <a href="<jetspeed:uriLookup type="Applications" />">Applications</a>
  &nbsp;&nbsp;&nbsp;
-->
  <a href="<jetspeed:uriLookup type="Logout" />">Logout</a>
  &nbsp;&nbsp;&nbsp;
  <a href="<jetspeed:uriLookup type="EditAccount" />">Edit Account (<jetspeed:info requestedInfo="UserName" />)</a>
  &nbsp;&nbsp;&nbsp;
  <br>
  <b>Server date:</b> <jetspeed:info requestedInfo="ServerDate" />
</div>
