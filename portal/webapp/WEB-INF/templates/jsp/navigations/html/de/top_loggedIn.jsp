<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<div>
  Welcome to Jetspeed -> 
  <a href="<jetspeed:uriLookup type="Home" />">Home</a>
  <br>
  <a href="<jetspeed:uriLookup type="Customize" />">Anpassen</a>
  &nbsp;&nbsp;&nbsp;
<!--
  <a href="<jetspeed:uriLookup type="Applications" />">Anwendungen</a>
  &nbsp;&nbsp;&nbsp;
-->
  <a href="<jetspeed:uriLookup type="Logout" />">Logout</a>
  &nbsp;&nbsp;&nbsp;
  <a href="<jetspeed:uriLookup type="EditAccount" />">Benutzerdaten bearbeiten (<jetspeed:info requestedInfo="UserName" />)</a>
  &nbsp;&nbsp;&nbsp;
  <br>
  <b>Server Datum:</b> <jetspeed:info requestedInfo="ServerDate" />
</div>
