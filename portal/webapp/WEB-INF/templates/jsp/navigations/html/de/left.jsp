<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td>
      <a href="<jetspeed:uriLookup type="Home" />">
        <small>Home</small>
      </a>
    </td>
  </tr>
  <tr>
    <td>
      <a href="<jetspeed:contentUri href="docs/" />">
        <small>Dokumentation</small>
      </a>
    </td>
  </tr>
  <tr>
    <td>
      <a href="<jetspeed:contentUri href="apidocs/" />">
        <small>API</small>
      </a>
    </td>
  </tr>
</table>
    
  </td>
  <td valign="top" rowspan="2">

