<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE rss PUBLIC "-//Netscape Communications//DTD RSS 0.91//EN"
              "http://my.netscape.com/publish/formats/rss-0.91.dtd">
<rss version="0.91">
   <channel>

   <title>xmlhack</title>
   <link>http://www.xmlhack.com</link>
   <description>Developer news from the XML community</description>
   <language>en-us</language>
   <copyright>Copyright 1999, xmlhack team.</copyright>
   <managingEditor>editor@xmlhack.com</managingEditor>
   <webMaster>webmaster@xmlhack.com</webMaster>

      <image>
        <title>xmlhack</title>
        <url>http://www.xmlhack.com/images/mynetscape88.gif</url>
        <link>http://www.xmlhack.com</link>
        <width>88</width>
        <height>31</height>
        <description>News, opinions, tips and issues concerning XML development</description>
      </image>

<item>
<title>Call for Extreme Participation</title>
<link>http://www.xmlhack.com/read.php?item=954</link>
<description>A call for participation has been issued for GCA's annual markup geek convention, Extreme Markup Languages, being held August 5-10 2001 in Montréal, Canada.</description>
<category>Community</category>
</item>
<item>
<title>IdooXoap reaches first stable release</title>
<link>http://www.xmlhack.com/read.php?item=953</link>
<description>IdooXoap, the Java SOAP implementation from Idoox (the folks behind Zvon, has reached its first stable release, including support for the latest XML Schema spec.</description>
<category>Protocols</category>
<category>Java</category>
</item>
<item>
<title>RDF concept reference</title>
<link>http://www.xmlhack.com/read.php?item=951</link>
<description>Dave Beckett, author of the Redland RDF framework, has collected together a reference for RDF and RDF schema concepts.</description>
<category>RDF</category>
</item>
<item>
<title>DOM test suite tests Adobe SVG, MSXML 3</title>
<link>http://www.xmlhack.com/read.php?item=950</link>
<description>The xmlconf project has released a modified version of NIST's DOM test suite for ECMAScript, adding support for testing of Adobe's SVG
Viewer, Xerces-COM and MSXML 3.</description>
<category>Tools</category>
<category>SVG</category>
</item>
<item>
<title>Second Candidate Recommendation for Canonical XML</title>
<link>http://www.xmlhack.com/read.php?item=949</link>
<description>Although formal review closed on 24 November, the XML Signature Working Group is still welcoming comment on the 'very stable' revised Candidate Recommendation of Canonical XML, Version 1.0.</description>
<category>W3C</category>
<category>IETF</category>
</item>
<item>
<title>XSLT 1.1 working draft surfaces</title>
<link>http://www.xmlhack.com/read.php?item=948</link>
<description>The W3C has published the first working draft of XSL Transformations (XSLT), Version 1.1, adding features and fixing errata.</description>
<category>XSLT</category>
<category>W3C</category>
</item>

</channel>
</rss>
